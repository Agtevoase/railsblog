class ArticlesController < ApplicationController
    before_action :set_article, only: [:edit, :update, :show, :destroy]

    def new
        @article = Article.new
    end

    def index
        @articles = Article.paginate(page: params[:page], per_page: 5)
    end

    def create
        if require_user
            return
        end
        @article = Article.new(article_params)
        @article.user = current_user
        @article.category_id = params[:article][:category_id]
        if @article.save
            flash[:success] = "Article was successfully created"
            redirect_to article_path(@article)
        else
            render 'new'
        end
    end

    def show
    end

    def edit
    end

    def update
        if current_user != @article.user
            flash[:danger] = "You may not edit this article"
            redirect_back(fallback_location: root_path)
            return
        end
        if @article.update(article_params)
            flash[:success] = "Article was updated"
            redirect_to article_path(@article)
        else
            flash[:notice] = "Article was not updated"
            render 'edit'
        end
    end

    def destroy
        if current_user != @article.user
            flash[:danger] = "You may not delete this article"
            redirect_back(fallback_location: root_path)
            return
        end
        @article.destroy
        flash[:notice] = "Article was deleted"
        redirect_to articles_path
    end

    private
    def article_params
        params.require(:article).permit(:title, :description)
    end

    def set_article
        @article = Article.find(params[:id])
    end
end
