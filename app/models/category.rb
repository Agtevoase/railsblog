class Category < ActiveRecord::Base
    has_many :articles
    validates :name, presence: true, uniqueness: { case_sensitive: true }, length: { minimum: 1, maximum: 25 }
end
